<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $argumento
 * @property string|null $foto
 * @property int|null $autor
 *
 * @property Autores $autor0
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor'], 'integer'],
            [['titulo', 'foto'], 'string', 'max' => 253],
            [['argumento'], 'string', 'max' => 511],
            [['autor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['autor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'argumento' => 'Argumento',
            'foto' => 'Foto',
            'autor' => 'Autor',
        ];
    }

    /**
     * Gets query for [[Autor0]].
     *
     * @return \yii\db\ActiveQuery|AutoresQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autores::className(), ['id' => 'autor']);
    }

    /**
     * {@inheritdoc}
     * @return LibrosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LibrosQuery(get_called_class());
    }
}
