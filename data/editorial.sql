﻿DROP DATABASE IF EXISTS editorial;
CREATE DATABASE editorial;

USE editorial;

CREATE TABLE autores (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(63),
  foto varchar(253)
  
  );

CREATE TABLE libros (
  id int AUTO_INCREMENT PRIMARY KEY,
  titulo varchar(253),
  argumento varchar(511),
  foto varchar (253),
  autor int
  );

ALTER TABLE libros
ADD FOREIGN KEY (autor) REFERENCES autores(id); 


INSERT INTO autores(nombre, foto) 
  VALUES 
  ('Luis Mendez',1),
  ('Pedro Jose López Moyano',2),
  ('Silvia San Sebastian',3),
  ('Daniel De Juana',4),
  ('Antonio Ferreira Lestido',5),
  ('Pilar López Quindós',6),
  ('Raul Hernández Vaz',7);

INSERT INTO libros (titulo, argumento,foto, autor) 
  VALUES
  ('Shicosis','marca Bic de toda la vida, hola',1,2),
  ('C++ programa en Microsoft',' Reloj nipones del penedes',2,1),
  ('PHP y My SQL','con cristales antireflectantes',3,5),
  ('Phithon 4','Marca Galgo',4,2),
  ('Scala','billetera,  monedero tarjetera de caballero ',5,3),
  ('Yii2','cuero rojo de mano',6,3),
  ('Excel 2019','Aos 25 pulgadas tft con altavoces y hdmi',7,2),
  ('Solidworks','JVC el de siempre',8,1),
  ('Autocad 2020','con separadores y de carton',9,4),
  ('Salud medio ambiental',' tipo bolsillo',10,5),
  ('Windows 20','Este aun no salió',11,4),
  ('Front Page 2003','Viejo editor de código HTML',12,6);



SELECT * FROM autores a;
SELECT * FROM libros l;
